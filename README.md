# document-translator (working name)

The goal is to have a GUI for opening images, highlighting/translating certain blocks of text, and annotating them with the translation.

![video of tool](https://gitlab.com/afiorillo/document-translator/raw/master/docs/screenshot.webm "Screenshot")


## Setup

Using [pipenv](#) as a package manager.

```bash
$ pipenv install
```

## Usage

Currently, to run it and open the sample image

```bash
$ pipenv run python translator.py
```

And that will open a Tkinter window.

The controls are:

- **Mouse Down:** Place the top-left vertex of a text selection
- **Mouse Up:** Place the bottom-right vertex of a text selection, and draw
- **Escape:** Undraw the current text selection
- **Return:** OCR & Google Translate the selected text, then draw it below the current selection.

## TODO

- Saving the output
- Handle images bigger than screen
- Manually reposition/resize translated text
- Configurable target language
- PDF support
- [BUG] Drawing boxes from bottom-right to top-left breaks (negative vertex differences)
