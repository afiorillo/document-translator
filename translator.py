from pathlib import Path
from tkinter import Tk, Frame, BOTH, Label
import logging
import json
from dataclasses import dataclass

from PIL import Image, ImageTk, ImageDraw, ImageFont
import pytesseract
import googletrans
import click

logging.basicConfig(format='[%(asctime)s] %(name)s %(levelname)-8s %(message)s', level=logging.DEBUG)
logging.getLogger('urllib3').setLevel(logging.ERROR)
logging.getLogger('PIL').setLevel(logging.ERROR)


class Window(Frame):
    logger = logging.getLogger('main-window')
    translator = googletrans.Translator()

    SELECTION_COLOR = (183, 8, 5, 64)
    DESTINATION_LANGUAGE = 'it'
    POINT_SIZE = 16
    ANNOTATION_FONT_FILE = 'DroidSerif-Bold.ttf'
    ANNOTATION_FONT = ImageFont.truetype(ANNOTATION_FONT_FILE, POINT_SIZE)
    ANNOTATION_COLOR = (239, 71, 4, 255)

    def __init__(self, fp, master=None, outdir=None):
        # setup window
        Frame.__init__(self, master=None)
        self.master = master
        self.pack(fill=BOTH, expand=1)
        self.master.bind('<ButtonPress-1>', lambda ev: self._mouse_down(self, ev))
        self.master.bind('<ButtonRelease-1>', lambda ev: self._mouse_up(self, ev))
        self.master.bind('<Return>', lambda ev: self._confirm_selection(self, ev))
        self.master.bind('<Escape>', lambda ev: self._undraw_selection())
        # data
        self.input_fp = fp
        if outdir is None:
            self.output_image_fp = fp.with_name(fp.stem + '.translated.jpg')
            self.output_annots_fp = fp.with_name(fp.stem + '.annotations.json')
        else:
            outdir = Path(outdir)
            self.output_image_fp = outdir.joinpath(fp.stem + '.trans' + fp.suffix)
            self.output_annots_fp = outdir.joinpath(fp.stem + '.annotations.json')
        self.raw_image = Image.open(fp).convert('RGBA')
        self.current_image = self.raw_image.copy()
        self.output_image = self.raw_image.copy()
        self.annotations = []
        
        self.w_width = self.raw_image.size[0]
        self.w_height = self.raw_image.size[1]
        self._selection_vertexes = [None, None, None, None]  # x1, y1, x2, y2

        self._redraw()

    def _redraw(self):
        render = ImageTk.PhotoImage(self.current_image)
        self.master.geometry(f'{self.w_width}x{self.w_height}')
        img = Label(self, image=render)
        img.image = render
        img.place(x=0, y=0)

    def _mouse_down(self, window, event, *args):
        x, y = event.x, event.y
        self.logger.debug(f'Down: {x}, {y}')
        self._selection_vertexes = [x, y, None, None]  # always resets

    def _mouse_up(self, window, event, *args):
        x, y = event.x, event.y
        self.logger.debug(f'Up: {x}, {y}')
        self._selection_vertexes[2], self._selection_vertexes[3] = x, y

        self._draw_selection()

    def _confirm_selection(self, window, event):
        roi = self._get_selected_roi()
        # ocr the string
        text = pytesseract.image_to_string(roi)
        lines = text.split('\n')
        self.logger.info(f'detected "{lines}"')
        # translate the string
        translations = [self.translator.translate(txt, dest=self.DESTINATION_LANGUAGE) for txt in lines]
        try:
            new_lines = [trans.text for trans in translations]
        except:
            self.logger.exception(f'unable to translate text')
            return
        self.logger.info(f'translated to "{new_lines}"')
        if len(new_lines) == 0 or all(l=='' for l in new_lines):
            self.logger.info(f'all translated text is empty, skipping')
            self._undraw_selection()
            return
        # draw the translated text
        # new_roi = self._draw_text(new_lines)
        trans_pos = [self._selection_vertexes[0], self._selection_vertexes[3]]
        # save annotation
        try:
            translated_lang = translations[0].dest
            original_lang = translations[0].src
        except IndexError:
            translated_lang = original_lang = None

        annot = {
            'original': {
                'text': text,
                'lang': original_lang,
                'position': self._selection_vertexes,
            },
            'translated': {
                'text': '\n'.join(new_lines),
                'lang': translated_lang,
                'position': trans_pos,
                'font_name': self.ANNOTATION_FONT_FILE,
                'font_size': self.POINT_SIZE,
                'font_color': self.ANNOTATION_COLOR,
            }
        }
        self.annotations.append(annot)
        with self.output_annots_fp.open('w', encoding='utf8') as fOut:
            json.dump(self.annotations, fOut, indent=2, sort_keys=True)
        
        # save image then draw it
        save_translated_image(self.input_fp, self.output_annots_fp, self.output_image_fp)
        self.current_image = Image.open(self.output_image_fp)
        self._redraw()

        # self.output_image.convert('RGB').save(self.output_image_fp)

    def _kill(self, *args):
        self.logger.fatal('received kill signal')
        self.master.quit()

    def _draw_selection(self):
        if any(vertex is None for vertex in self._selection_vertexes):
            self.logger.warn('cannot draw selection without all vertices')
            self._undraw_selection()
            return

        self.current_image = self.output_image.copy()
        overlay = Image.new('RGBA', self.current_image.size, self.SELECTION_COLOR[:-1] + (0, ))
        draw = ImageDraw.Draw(overlay)
        draw.rectangle(self._selection_vertexes, fill=self.SELECTION_COLOR)

        self.current_image = Image.alpha_composite(self.current_image, overlay)
        self._redraw()

    def _undraw_selection(self):
        self.current_image = self.output_image.copy()
        self._redraw()

    def _get_selected_roi(self):
        return self.raw_image.crop(self._selection_vertexes)


def save_translated_image(original_image_fp, annotations_fp, new_image_fp=None):
    original_image_fp = Path(original_image_fp).resolve()
    annotations_fp = Path(annotations_fp).resolve()
    if new_image_fp is None:
        new_image_fp = original_image_fp.with_name(original_image_fp.stem + '.trans' + original_image_fp.suffix)

    img = Image.open(original_image_fp).convert('RGBA')
    out_img = img.copy()
    annots = json.loads(annotations_fp.read_text())

    for annot in annots:
        # extract the text, font, etc info
        fontname = annot['translated']['font_name']
        fontsize = annot['translated']['font_size']
        font = ImageFont.truetype(fontname, fontsize)
        fontcolor = tuple(annot['translated']['font_color'])

        lines_of_text = annot['translated']['text'].split('\n')
        nw = annot['translated']['position']

        # draw an overlay on top
        overlay = Image.new('RGBA', out_img.size, (255, 255, 255, 0))
        d = ImageDraw.Draw(overlay)

        line_sizes = [d.textsize(line, font=font) for line in lines_of_text]
        height = sum(sz[1] for sz in line_sizes)
        line_height = height // len(lines_of_text)
        nw[1] += height

        for indx, line in enumerate(lines_of_text):
            d.text((nw[0], nw[1]+((indx-1)*line_height)), line, fill=fontcolor)
        out_img = Image.alpha_composite(out_img, overlay)

    out_img.convert('RGB').save(new_image_fp)

@click.group()
def main():
    pass


@main.command('draw')
@click.argument('filename')
@click.option('--output-dir')
def draw(filename, output_dir=None):
    root = Tk()
    Window(Path(filename), master=root, outdir=output_dir)
    root.mainloop()


@main.command('annotate')
@click.argument('imagefile')
@click.argument('annotsfile')
def annotate(imagefile, annotsfile):
    save_translated_image(imagefile, annotsfile)


if __name__ == '__main__':
    main()